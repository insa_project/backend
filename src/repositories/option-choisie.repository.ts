import {DefaultCrudRepository} from '@loopback/repository';
import {OptionChoisie, OptionChoisieRelations} from '../models';
import {OpenvisioDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OptionChoisieRepository extends DefaultCrudRepository<
  OptionChoisie,
  typeof OptionChoisie.prototype.id_option,
  OptionChoisieRelations
> {
  constructor(
    @inject('datasources.openvisio') dataSource: OpenvisioDataSource,
  ) {
    super(OptionChoisie, dataSource);
  }
}
