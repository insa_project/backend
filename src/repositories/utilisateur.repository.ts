import {DefaultCrudRepository} from '@loopback/repository';
import {Utilisateur, UtilisateurRelations} from '../models';
import {OpenvisioDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UtilisateurRepository extends DefaultCrudRepository<
  Utilisateur,
  typeof Utilisateur.prototype.ID,
  UtilisateurRelations
> {
  constructor(
    @inject('datasources.openvisio') dataSource: OpenvisioDataSource,
  ) {
    super(Utilisateur, dataSource);
  }
}
