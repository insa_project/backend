import {DefaultCrudRepository} from '@loopback/repository';
import {Etudiant, EtudiantRelations} from '../models';
import {OpenvisioDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EtudiantRepository extends DefaultCrudRepository<
  Etudiant,
  typeof Etudiant.prototype.id_user,
  EtudiantRelations
> {
  constructor(
    @inject('datasources.openvisio') dataSource: OpenvisioDataSource,
  ) {
    super(Etudiant, dataSource);
  }
}
