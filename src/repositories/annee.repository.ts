import {DefaultCrudRepository} from '@loopback/repository';
import {Annee, AnneeRelations} from '../models';
import {OpenvisioDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AnneeRepository extends DefaultCrudRepository<
  Annee,
  typeof Annee.prototype.id_annee,
  AnneeRelations
> {
  constructor(
    @inject('datasources.openvisio') dataSource: OpenvisioDataSource,
  ) {
    super(Annee, dataSource);
  }
}
