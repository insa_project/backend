import {DefaultCrudRepository} from '@loopback/repository';
import {Filiere, FiliereRelations} from '../models';
import {OpenvisioDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class FiliereRepository extends DefaultCrudRepository<
  Filiere,
  typeof Filiere.prototype.id_filiere,
  FiliereRelations
> {
  constructor(
    @inject('datasources.openvisio') dataSource: OpenvisioDataSource,
  ) {
    super(Filiere, dataSource);
  }
}
