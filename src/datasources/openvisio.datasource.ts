import { inject, lifeCycleObserver, LifeCycleObserver } from "@loopback/core";
import { juggler } from "@loopback/repository";

const config = {
  name: "openvisio",
  connector: "mysql",
  url: "",
  host: "192.168.64.2",
  port: 3306,
  user: "user",
  password: "password",
  database: "openvisio",
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver("datasource")
export class OpenvisioDataSource
  extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = "openvisio";
  static readonly defaultConfig = config;

  constructor(
    @inject("datasources.config.openvisio", { optional: true })
    dsConfig: object = config
  ) {
    super(dsConfig);
  }
}
