import { ApplicationConfig, OpenvisioApplication } from "./application";
import { SocketIoApp } from "./application.socketio";
import fs from "fs";
export * from "./application";
export * from "./application.socketio";
export * from "./jwt-authentication-component";
export * from "./keys";
export * from "./services";
export * from "./types";

export async function mainRest(options: ApplicationConfig = {}) {
  const app = new OpenvisioApplication(options);

  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}

export async function mainSocket(options: ApplicationConfig = {}) {
  const app = new SocketIoApp(options);
  await app.boot();
  await app.start();

  const url = app.socketServer.url;
  console.log(`Server is running at ${url}`);

  return app;
}

function getIPAddress() {
  var interfaces = require("os").networkInterfaces();
  for (var devName in interfaces) {
    var iface = interfaces[devName];

    for (var i = 0; i < iface.length; i++) {
      var alias = iface[i];
      if (
        alias.family === "IPv4" &&
        alias.address !== "127.0.0.1" &&
        !alias.internal
      )
        return alias.address;
    }
  }
  return "0.0.0.0";
}

if (require.main === module) {
  // Run the application
  process.env.HOST = getIPAddress();
  process.env.PORT_REST = "3000";
  process.env.PORT_SOCKET = "3001";
  const configRest = {
    rest: {
      port: +(process.env.PORT_REST ?? 3000),
      host: process.env.HOST,
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
      protocol: "https",
      key: fs.readFileSync("src/keys/server.key"),
      cert: fs.readFileSync("src/keys/server.crt"),
    },
  };
  const configSocket = {
    httpServerOptions: {
      port: +(process.env.PORT_SOCKET ?? 3000),
      host: process.env.HOST,
      protocol: "https",
      key: fs.readFileSync("src/keys/server.key"),
      cert: fs.readFileSync("src/keys/server.crt"),
    },
  };
  mainRest(configRest).catch((err) => {
    console.error("Cannot start the rest application.", err);
    process.exit(1);
  });
  mainSocket(configSocket).catch((err) => {
    console.error("Cannot start the socket application.", err);
    process.exit(1);
  });
}
