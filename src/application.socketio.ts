// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/example-socketio
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {BootMixin, } from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {SocketIoApplication} from '@loopback/socketio';
import { RepositoryMixin } from "@loopback/repository";
import { ServiceMixin } from "@loopback/service-proxy";
import debugFactory from 'debug';
import {SocketIoController} from './controllers';

const debug = debugFactory('loopback:example:socketio:demo');

export {ApplicationConfig};
SocketIoApplication
export class SocketIoApp extends BootMixin(
  ServiceMixin(RepositoryMixin(SocketIoApplication))
)  {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    this.projectRoot = __dirname;

    this.socketServer.use((socket, next) => {
      debug('Global middleware - socket:', socket.id);
      next();
    });

    const ns = this.socketServer.route(SocketIoController);
    ns.use((socket: any, next: any) => {
      debug(
        'Middleware for namespace %s - socket: %s',
        socket.nsp.name,
        socket.id,
      );
      next();
    });
  }
}