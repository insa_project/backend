import { Entity, model, property } from "@loopback/repository";

@model({
  settings: {
    idInjection: false,
    mysql: { schema: "BD_OpenVisio", table: "Option_choisie" },
    foreignKeys: {
      fk_option_id_annee: {
        name: "fk_option_id_annee",
        entity: "Annee",
        entityKey: "id_annee",
        foreignKey: "id_annee",
      },

      fk_option_id_filiere: {
        name: "fk_option_id_filiere",
        entity: "Filiere",
        entityKey: "id_filiere",
        foreignKey: "id_filiere",
      },
    },
  },
})
export class OptionChoisie extends Entity {
  @property({
    type: "number",
    required: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: {
      columnName: "id_option",
      dataType: "int",
      dataLength: null,
      dataPrecision: 10,
      dataScale: 0,
      nullable: "N",
    },
  })
  idOption: number;

  @property({
    type: "string",
    required: true,
    length: 5,
    mysql: {
      columnName: "valeur",
      dataType: "VARCHAR",
      dataLength: 5,
      dataPrecision: null,
      dataScale: null,
      nullable: "N",
    },
  })
  valeur: string;

  @property({
    type: "number",
    required: true,
    precision: 10,
    scale: 0,
    mysql: {
      columnName: "id_annee",
      dataType: "int",
      dataLength: null,
      dataPrecision: 10,
      dataScale: 0,
      nullable: "N",
    },
  })
  idAnnee: number;

  @property({
    type: "number",
    required: true,
    precision: 10,
    scale: 0,
    mysql: {
      columnName: "id_filiere",
      dataType: "int",
      dataLength: null,
      dataPrecision: 10,
      dataScale: 0,
      nullable: "N",
    },
  })
  idFiliere: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<OptionChoisie>) {
    super(data);
  }
}

export interface OptionChoisieRelations {
  // describe navigational properties here
}

export type OptionChoisieWithRelations = OptionChoisie & OptionChoisieRelations;
