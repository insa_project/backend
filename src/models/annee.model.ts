import { Entity, model, property } from "@loopback/repository";

@model({
  settings: {
    idInjection: false,
    mysql: { schema: "BD_OpenVisio", table: "Annee" },
  },
})
export class Annee extends Entity {
  @property({
    type: "number",
    required: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: {
      columnName: "id_annee",
      dataType: "int",
      dataLength: null,
      dataPrecision: 10,
      dataScale: 0,
      nullable: "N",
    },
  })
  idAnnee: number;

  @property({
    type: "string",
    required: true,
    length: 2,
    mysql: {
      columnName: "Valeur",
      dataType: "VARCHAR",
      dataLength: 2,
      dataPrecision: null,
      dataScale: null,
      nullable: "N",
    },
  })
  valeur: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Annee>) {
    super(data);
  }
}

export interface AnneeRelations {
  // describe navigational properties here
}

export type AnneeWithRelations = Annee & AnneeRelations;
