import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Annee} from '../models';
import {AnneeRepository} from '../repositories';

export class AnneeController {
  constructor(
    @repository(AnneeRepository)
    public anneeRepository : AnneeRepository,
  ) {}

  @post('/annees', {
    responses: {
      '200': {
        description: 'Annee model instance',
        content: {'application/json': {schema: getModelSchemaRef(Annee)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Annee, {
            title: 'NewAnnee',
            
          }),
        },
      },
    })
    annee: Annee,
  ): Promise<Annee> {
    return this.anneeRepository.create(annee);
  }

  @get('/annees/count', {
    responses: {
      '200': {
        description: 'Annee model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Annee) where?: Where<Annee>,
  ): Promise<Count> {
    return this.anneeRepository.count(where);
  }

  @get('/annees', {
    responses: {
      '200': {
        description: 'Array of Annee model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Annee, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Annee) filter?: Filter<Annee>,
  ): Promise<Annee[]> {
    return this.anneeRepository.find(filter);
  }

  @patch('/annees', {
    responses: {
      '200': {
        description: 'Annee PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Annee, {partial: true}),
        },
      },
    })
    annee: Annee,
    @param.where(Annee) where?: Where<Annee>,
  ): Promise<Count> {
    return this.anneeRepository.updateAll(annee, where);
  }

  @get('/annees/{id}', {
    responses: {
      '200': {
        description: 'Annee model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Annee, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Annee, {exclude: 'where'}) filter?: FilterExcludingWhere<Annee>
  ): Promise<Annee> {
    return this.anneeRepository.findById(id, filter);
  }

  @patch('/annees/{id}', {
    responses: {
      '204': {
        description: 'Annee PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Annee, {partial: true}),
        },
      },
    })
    annee: Annee,
  ): Promise<void> {
    await this.anneeRepository.updateById(id, annee);
  }

  @put('/annees/{id}', {
    responses: {
      '204': {
        description: 'Annee PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() annee: Annee,
  ): Promise<void> {
    await this.anneeRepository.replaceById(id, annee);
  }

  @del('/annees/{id}', {
    responses: {
      '204': {
        description: 'Annee DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.anneeRepository.deleteById(id);
  }
}
