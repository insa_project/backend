import {
  Request,
  RestBindings,
  post,
  get,
  requestBody,
  ResponseObject,
} from "@loopback/rest";
import { inject } from "@loopback/core";
export const configRequest = {
  description: "COnfiguration to request",
  required: true,
  content: {
    "application/json": { schema: { exposeState: "boolean" } },
  },
};

const { exec } = require("child_process");

const EXPOSE_RESPONSE: ResponseObject = {
  description: "Expose Server",
  content: {
    "application/json": {
      schema: {
        type: "object",
        title: "ExposeResponse",
        properties: {
          publicIp: { type: "string" },
        },
      },
    },
  },
};

const IP_RESPONSE: ResponseObject = {
  description: "Expose Server",
  content: {
    "application/json": {
      schema: {
        type: "object",
        title: "ExposeResponse",
        properties: {
          publicIp: { type: "string" },
          internalIp: { type: "string" },
        },
      },
    },
  },
};

export class ConfigurationController {
  constructor(@inject(RestBindings.Http.REQUEST) private req: Request) {}

  async getExternalIp(state: boolean) {
    return new Promise<string>((res, err) => {
      if (state) {
        exec(
          "upnpc -r 3000 3000 UDP 3000 3000 TCP 3001 3001 UDP 3001 3001 TCP",
          (error: any, stdout: any, stderr: any) => {
            if (error) {
              err(`error: ${error}`);
              return;
            }
            if (stderr) {
              err(`stderr: ${stderr}`);
              return;
            } else {
              console.log(`stdout: ${stdout}`);
              exec(
                "upnpc -l | grep ExternalIPAddress | cut -d ' ' -f 3",
                (error: any, stdout: string, stderr: any) => {
                  if (error) err(`error: ${error.message}`);
                  if (stderr) err(`stderr: ${stderr}`);
                  else res(stdout.replace("\n", ""));
                }
              );
            }
          }
        );
      } else {
        exec("upnpc -d 3000 UDP", (error: any, stdout: any, stderr: any) => {
          if (error) {
            err(`error: ${error.message}`);
            return;
          }
          if (stderr) {
            err(`stderr: ${stderr}`);
            return;
          }
        });
        exec("upnpc -d 3000 TCP", (error: any, stdout: any, stderr: any) => {
          if (error) {
            err(`error: ${error.message}`);
            return;
          }
          if (stderr) {
            err(`stderr: ${stderr}`);
            return;
          } else {
            res("");
            return;
          }
        });
        exec("upnpc -d 3001 TCP", (error: any, stdout: any, stderr: any) => {
          if (error) {
            err(`error: ${error.message}`);
            return;
          }
          if (stderr) {
            err(`stderr: ${stderr}`);
            return;
          } else {
            res("");
            return;
          }
        });
        exec("upnpc -d 3001 UDP", (error: any, stdout: any, stderr: any) => {
          if (error) {
            err(`error: ${error.message}`);
            return;
          }
          if (stderr) {
            err(`stderr: ${stderr}`);
            return;
          } else {
            res("");
            return;
          }
        });
      }
    });
  }

  async getIp() {
    return new Promise<string>((res, err) => {
      exec(
        "upnpc -l | grep ExternalIPAddress | cut -d ' ' -f 3",
        (error: any, stdout: string, stderr: any) => {
          if (error) err(`error: ${error.message}`);
          if (stderr) err(`stderr: ${stderr}`);
          else res(stdout.replace("\n", ""));
        }
      );
    });
  }

  @post("/config/expose", {
    responses: {
      "200": EXPOSE_RESPONSE,
    },
  })
  async expose(
    @requestBody({
      content: {
        "application/json": {
          schema: {
            state: Boolean,
          },
        },
      },
    })
    stateExpose: {
      state: boolean;
    }
  ): Promise<object> {
    let publicIp = await this.getExternalIp(stateExpose.state);
    return { publicIp: publicIp };
  }

  @get("/config/ipaddress", {
    responses: {
      "200": IP_RESPONSE,
    },
  })
  async ipAddress(): Promise<object> {
    let publicIp = await this.getIp();
    return { publicIp: publicIp, internalIp: process.env.HOST };
  }
}
