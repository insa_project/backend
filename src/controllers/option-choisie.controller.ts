import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {OptionChoisie} from '../models';
import {OptionChoisieRepository} from '../repositories';

export class OptionChoisieController {
  constructor(
    @repository(OptionChoisieRepository)
    public optionChoisieRepository : OptionChoisieRepository,
  ) {}

  @post('/option-choisies', {
    responses: {
      '200': {
        description: 'OptionChoisie model instance',
        content: {'application/json': {schema: getModelSchemaRef(OptionChoisie)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OptionChoisie, {
            title: 'NewOptionChoisie',
            
          }),
        },
      },
    })
    optionChoisie: OptionChoisie,
  ): Promise<OptionChoisie> {
    return this.optionChoisieRepository.create(optionChoisie);
  }

  @get('/option-choisies/count', {
    responses: {
      '200': {
        description: 'OptionChoisie model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(OptionChoisie) where?: Where<OptionChoisie>,
  ): Promise<Count> {
    return this.optionChoisieRepository.count(where);
  }

  @get('/option-choisies', {
    responses: {
      '200': {
        description: 'Array of OptionChoisie model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(OptionChoisie, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(OptionChoisie) filter?: Filter<OptionChoisie>,
  ): Promise<OptionChoisie[]> {
    return this.optionChoisieRepository.find(filter);
  }

  @patch('/option-choisies', {
    responses: {
      '200': {
        description: 'OptionChoisie PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OptionChoisie, {partial: true}),
        },
      },
    })
    optionChoisie: OptionChoisie,
    @param.where(OptionChoisie) where?: Where<OptionChoisie>,
  ): Promise<Count> {
    return this.optionChoisieRepository.updateAll(optionChoisie, where);
  }

  @get('/option-choisies/{id}', {
    responses: {
      '200': {
        description: 'OptionChoisie model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(OptionChoisie, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(OptionChoisie, {exclude: 'where'}) filter?: FilterExcludingWhere<OptionChoisie>
  ): Promise<OptionChoisie> {
    return this.optionChoisieRepository.findById(id, filter);
  }

  @patch('/option-choisies/{id}', {
    responses: {
      '204': {
        description: 'OptionChoisie PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OptionChoisie, {partial: true}),
        },
      },
    })
    optionChoisie: OptionChoisie,
  ): Promise<void> {
    await this.optionChoisieRepository.updateById(id, optionChoisie);
  }

  @put('/option-choisies/{id}', {
    responses: {
      '204': {
        description: 'OptionChoisie PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() optionChoisie: OptionChoisie,
  ): Promise<void> {
    await this.optionChoisieRepository.replaceById(id, optionChoisie);
  }

  @del('/option-choisies/{id}', {
    responses: {
      '204': {
        description: 'OptionChoisie DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.optionChoisieRepository.deleteById(id);
  }
}
